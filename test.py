from sklearn.metrics import accuracy_score

from utils import load_pickle

if __name__ == '__main__':
    X_test = load_pickle('out/X_test.pl')
    y_train = load_pickle('out/y_train.pl')
    y_test = load_pickle('out/y_test.pl')

    # Load the model
    clf = load_pickle('out/clf.pl')
    clf_svm = load_pickle('out/clf_svm.pl')

    y_pred = clf.predict(X_test)
    y_pred_svm = clf_svm.predict(X_test)

    print("Accuracy DecisionTreeClassifier: ", accuracy_score(y_test, y_pred) * 100)
    print("Accuracy SVM: ", accuracy_score(y_test, y_pred_svm) * 100)
