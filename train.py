import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier, export_graphviz

from utils import create_dir, save_pickle

if __name__ == '__main__':
    balance_data = pd.read_csv(
        'https://archive.ics.uci.edu/ml/machine-learning-databases/lymphography/lymphography.data',
        sep=',', header=None)

    print("Dataset Shape:: ", balance_data.shape)

    X = balance_data.values[:, 1:]
    y = balance_data.values[:, 0]

    # Split and shuffle the data
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=100)

    # Train the classifier
    clf = DecisionTreeClassifier(criterion="gini", random_state=100, max_depth=4, min_samples_leaf=5)
    clf.fit(X_train, y_train)

    # Train an alternative classifier
    clf_svm = OneVsRestClassifier(LinearSVC(max_iter=100000), n_jobs=1)
    clf_svm.fit(X_train, y_train)

    # Create output directory
    create_dir('out')

    # Save the data
    save_pickle('out/X_train.pl', X_train)
    save_pickle('out/X_test.pl', X_test)
    save_pickle('out/y_train.pl', y_train)
    save_pickle('out/y_test.pl', y_test)

    # Save the model
    save_pickle('out/clf.pl', clf)
    save_pickle('out/clf_svm.pl', clf_svm)

    export_graphviz(clf, out_file='out/clf.dot')
